# primeros 2 bytes :
# 00 = login      ,
# 01 = posicion
# 02 = evento
#
########### caso login:
# respuesta : 100
#
########### caso posicion:
# respuesta: si es valido=111, si es invalido=110
# dato | bytes
# id = 2-9
# timestamp = 10-19
# sur o norte = 20 (SUR vuelve negativo la coordenada)
# lat = 21-27
# oeste o este = 28 (OESTE vuelve negativo la coordenada)
# lon = 29-35
# validez = 36 (A=valido,V=invalido)
# velocidad = 37-38
#
#
#
########### caso evento
#respusta : 120
# dato | bytes
# output_code = 2-3
# output_value = 4-5
#require "active_support/all"
require 'date'
class Decoder

	@id
	@datetime
	@speed
	@valid
	@lat
	@lon
	@response
	@pack
	@output_code
	@output_value

	attr_accessor :id, :datetime, :speed, :valid, :lat, :lon, :response, :output_code, :output_value

	def initialize

	end
	def read(pack)
			@pack = pack
			caso = @pack[0..1]
		if  caso=="01"
			@id = @pack[2..9]
			fecha = Time.at(@pack[10..19].to_i).to_datetime.strftime("%Y-%m-%d %H:%M:%S")
			@datetime = fecha
			@speed = @pack[37..38]
			lonDir = @pack[20]
			latDir = @pack[28]
			@lat = @pack[21..27] # -W E
			@lon = @pack[29..35] # N -S
			case latDir
				when "W"
						@lat = @lat.to_f * -1
				when "E"
						@lat = @lat.to_f
				else
						@lat = 0
				end
			case lonDir
					when "S"
						@lon = @lon.to_f * -1
					when "N"
						@lon = @lon.to_f
			  	else
						@lon = 0
				end
			@valid = @pack[36]
			case @valid
					when "A"
						@response = "111"
					when "V"
						@response = "110"
					else
						@response = "0"
				end
			elsif caso=="02"
				@output_code = @pack[2..3]
				@output_value = @pack[4..5]
				@response = "120"
			else
				@pack = "00"
			end
			self
		end

	def login
				if @pack == "00"
					@response = "100"
					puts "OK login respuesta: #{@response}"
					return true
				end
					return false
	end
end
